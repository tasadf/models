File: "sdf3-tool-mpeg-4-fsmsadf-graph.xml"
hosts the SDF3 tool model of the MPEG-4 decoder


File: "uppaal-example-fsmsadf-graph.xml"
hosts the UPPAAL model of an example FSM-SADFG

 
File: "uppaal-mpeg-4-fsmsadf-graph.xml"
hosts the UPPAAL model of the MPEG-4 FSM-SADFG


Subfolder "~/queries" contains replicas of
the MPEG-4 UPPAAL model used for queries (one file per query,
filename designates the name of the query)

NOTE: 
Use UPPAAL v.4.1.19 (or the latest version) that merges query files with the model file. 
Otherwise, you won't be able to see the queries, as older UPPAAL versions (<= 4.1.18)
require separate .q files.
You can downolad the latest version of UPPAAL from
http://www.uppaal.org/


