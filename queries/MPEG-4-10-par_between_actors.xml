<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE nta PUBLIC '-//Uppaal Team//DTD Flat System 1.1//EN' 'http://www.it.uu.se/research/group/darts/uppaal/flat-1_2.dtd'>
<nta>
	<declaration>// mladen.skelin@itk.ntnu.no
// Place global declarations here.
// The numbers for rates and kernel execution times were taken from the MNEMEE report
// The FSM was taken from the paper of Stuijk et al.: SADF: modeling, Analysis and Implementation of Dynamic Applications



// Definitions of constants for the MPEG-4 FSM-SADF case study
// {

const int NUM_DATA_BUFFS = 6;
const int NUM_CTL_BUFFS	= 4;

const int NUM_OF_SCENARIOS = 9;

const int KER_MAX_DATA_INPUTS = 2;
// Always in FSM-SADF
const int KER_MAX_CTL_INPUTS = 1;

const int KER_MAX_DATA_OUTPUTS = 2;

const int DET_MAX_DATA_INPUTS = 1;
const int DET_MAX_CTL_OUTPUTS =  NUM_CTL_BUFFS;

const int NUM_OF_KERNELS = 4;

const int CTL_BUFF_SIZE = 511;

// }


// Definitions of data types for the MPEG-4 FSM-SADF case study
// {
// The queue for colored tokens
// For overflow detection at run-time
typedef int[0,CTL_BUFF_SIZE] CTL_LEN;
typedef struct
{
	int queue[CTL_BUFF_SIZE+1];
    // This is the actual len... To be used for queries, etc...
	CTL_LEN len;	
}CTL_QUEUE;

// Kernel configuration
typedef struct
{
	// Number of input data buffers
	int num_inp_data_buffs;
    // Array of ID's (refferences the global var) of the input buffers for a specific actor
	int inp_data_buffs[KER_MAX_DATA_INPUTS];
	
	// Kernel does not necesarily have a control port,
	// it means that it always operates in the sam scenario
	// however, for compactness it can be equipped with a trivial detector
	// without infulencing the preformance properties
    // ID (refferences the global var) of the control buffer for a specific actor
	int inp_ctl_buff;
	
    // The same principle for output buffers as above described
	int num_out_data_buffs;
	int out_data_buffs[KER_MAX_DATA_OUTPUTS];
}KERNEL;

// Detector configuration
typedef struct
{
	// A detector has dataflow inputs and control outputs
	// No dataflow outputs are mentioned in the operational semantics from the MNEMEE report (see detector end transition)
    // Number of input df buffers
	int num_inp_data_buffs;
    // And their ID's
	int inp_data_buffs[DET_MAX_DATA_INPUTS];
	
}DETECTOR;

typedef struct
{
	int     p_rate;
	int     c_rate;
}BUFF;

typedef struct
{
    // Number of tokens present
    int     stateX[NUM_OF_SCENARIOS+1];
    // Just for max-buff ocupancy query, no meaning for the model itself
    int     total;
    // Can overtaking take place on this channel
    bool    overtaking;
}DATA_BUFFER;

typedef struct
{
    // This is a scenario configuration
	// For each edge the consmption and production
	BUFF ctl_buff_cfg[NUM_CTL_BUFFS];
	BUFF data_buff_cfg[NUM_DATA_BUFFS];
    // Array of actor delays ordered by the ID's
	int  kernel_delay[NUM_OF_KERNELS];
	int  detector_delay; 
	
}SCENARIO;

// }

// Instances of the data types for the MPEG-4 FSM-SADF case study
// {
// UPPAAL sets this to 0
CTL_QUEUE	control_buffers[NUM_CTL_BUFFS];

// These are the data buffers
// These are split over scenarios
// The last one is the "default scenario", i.e. the buffer of the channel where there is no overtaking
// The initial state is assigned to that channel
// On the "overtaking" channel no initial tokens are allowed
//int data_buffers[NUM_DATA_BUFFS][NUM_OF_SCENARIOS+1] = { {0,0,2}, {0,0,0}, {0,0,2} };
// Remove self-edges
//int data_buffers[NUM_DATA_BUFFS][NUM_OF_SCENARIOS+1] = {{0,0,2}, {0,0,0}, {0,0,2}};
// Initial tokens need to be put is a special container
// as there is no way of distributing them over replicated "scenario" channels
//int data_buffers_initial[NUM_DATA_BUFFS] = {2,2,0,2,1};
// This with the initial buffer does not work
// set it to zero and assume some history of the sytem
// int data_buffers_initial[NUM_DATA_BUFFS] = {0,0,0,0,0};

// Initial state is here
//int data_buffers[NUM_DATA_BUFFS] = {0,0,1,0,0,3};

// Whenever it's true for overtaking, all initializer entries must be = 0
// Condition for FIFO policy
DATA_BUFFER data_buffers_redef[NUM_DATA_BUFFS] = {
    {{0,0,0,0,0,0,0,0,0,0},0,false},
    {{0,0,0,0,0,0,0,0,0,0},0,false},
    {{0,0,0,0,0,0,0,0,0,1},1,false},
    {{0,0,0,0,0,0,0,0,0,0},0,false},
    {{0,0,0,0,0,0,0,0,0,0},0,false},
    {{0,0,0,0,0,0,0,0,0,3},3,false}
    };

// Graph structure (detectors)
const KERNEL 		kernels[NUM_OF_KERNELS] = {
	{0, {0,0}, 0, 2, {0,1}},
	{1, {0,0}, 1, 1, {4,0}},
	{2, {3,4}, 3, 2, {2,5}},
	{2, {1,2}, 2, 1, {3,0}}
	};

// Graph structure (kernels)
const DETECTOR 	detector = { 1, {5} };

// Scenario table (the configurations)
const SCENARIO scenarios[NUM_OF_SCENARIOS]={
	{{{99,1},{99,1},{1,1},{1,1}}, {{1,1},{0,0},{1,1},{1,1},{1,99},{1,1}}, {40,17,350,0}, 0},
	{{{1,1},{1,1},{1,1},{1,1}}, {{0,0},{0,0},{1,1},{1,1},{0,0},{1,1}}, {0,0,0,0}, 0},
	{{{30,1},{30,1},{1,1},{1,1}}, {{1,1},{1,30},{1,1},{1,1},{1,30},{1,1}}, {40,17,250,90}, 0},
	{{{40,1},{40,1},{1,1},{1,1}}, {{1,1},{1,40},{1,1},{1,1},{1,40},{1,1}}, {40,17,250,145}, 0},
	{{{50,1},{50,1},{1,1},{1,1}}, {{1,1},{1,50},{1,1},{1,1},{1,50},{1,1}}, {40,17,250,190}, 0},
	{{{60,1},{60,1},{1,1},{1,1}}, {{1,1},{1,60},{1,1},{1,1},{1,60},{1,1}}, {40,17,300,235}, 0},
	{{{70,1},{70,1},{1,1},{1,1}}, {{1,1},{1,70},{1,1},{1,1},{1,70},{1,1}}, {40,17,320,265}, 0},
	{{{80,1},{80,1},{1,1},{1,1}}, {{1,1},{1,80},{1,1},{1,1},{1,80},{1,1}}, {40,17,320,310}, 0},
	{{{99,1},{99,1},{1,1},{1,1}}, {{1,1},{1,99},{1,1},{1,1},{1,99},{1,1}}, {40,17,320,390}, 0}
	
};

// }


// Methods
//{

bool kernel_data_tokens_available(int kernel_id, int scenario_id)
{
	int i=0;
    int tmp=0;
    int index;
	
	for(i=0; i &lt; kernels[kernel_id].num_inp_data_buffs; i++)
	{
        // How many do we have there
        tmp = 0;
        
        if( data_buffers_redef[ kernels[kernel_id].inp_data_buffs[i] ].overtaking )
        {
            index = scenario_id;
        }
        else
        {
            index = NUM_OF_SCENARIOS;
        }
        
        tmp = data_buffers_redef[ kernels[kernel_id].inp_data_buffs[i] ].stateX[index];

		if( tmp  &lt; scenarios[scenario_id].data_buff_cfg[ kernels[kernel_id].inp_data_buffs[i] ].c_rate  )
		{
			return false;
		}
	}
	
	return true;	
}

bool detector_data_tokens_available(int scenario_id)
{
	int i=0;
    int tmp=0;
    int index;

	
	for(i=0; i &lt; detector.num_inp_data_buffs; i++)
	{
        // How many do we have there
        tmp = 0;
        if( data_buffers_redef[detector.inp_data_buffs[i] ].overtaking )
        {
            index = scenario_id;
        }
        else
        {
            index = NUM_OF_SCENARIOS;
        }

        tmp = data_buffers_redef[ detector.inp_data_buffs[i] ].stateX[index];
		if( tmp &lt; scenarios[scenario_id].data_buff_cfg[ detector.inp_data_buffs[i] ].c_rate  )
		{
			return false;
		}
	}
	
	return true;	
}

void detector_consume_data_tokens(int scenario_id)
{
	int i=0;
    int tmp=0;
    int index;
	
	for(i=0; i &lt; detector.num_inp_data_buffs; i++)
	{
        // Default 
        tmp = scenarios[scenario_id].data_buff_cfg[ detector.inp_data_buffs[i] ].c_rate;

        if( data_buffers_redef[ detector.inp_data_buffs[i] ].overtaking )
        {
            index = scenario_id;
        }
        else
        {
            index = NUM_OF_SCENARIOS;
        }  

		data_buffers_redef[detector.inp_data_buffs[i]].stateX[index] -= tmp;
        data_buffers_redef[detector.inp_data_buffs[i]].total -= tmp;
	}
}

void kernel_consume_data_tokens(int kernel_id, int scenario_id)
{
	int i=0;
    int tmp=0;
    int index;
	
    for(i=0; i &lt; kernels[kernel_id].num_inp_data_buffs; i++)
	{
        // Default
        tmp = scenarios[scenario_id].data_buff_cfg[ kernels[kernel_id].inp_data_buffs[i] ].c_rate;
        
        if( data_buffers_redef[ kernels[kernel_id].inp_data_buffs[i] ].overtaking )
        {
            index = scenario_id;
        }
        else
        {
            index = NUM_OF_SCENARIOS;
        }       

		data_buffers_redef[ kernels[kernel_id].inp_data_buffs[i] ].stateX[index] -=  tmp;
        data_buffers_redef[ kernels[kernel_id].inp_data_buffs[i] ].total -=  tmp;
	}
}

void kernel_produce_data_tokens(int kernel_id, int scenario_id)
{
	int i=0;
    int index;
	
	for(i=0; i &lt; kernels[kernel_id].num_out_data_buffs; i++)
	{
        if( data_buffers_redef[ kernels[kernel_id].out_data_buffs[i] ].overtaking )
        {
            index = scenario_id;
        }
        else
        {
            index = NUM_OF_SCENARIOS;
        }

		data_buffers_redef[ kernels[kernel_id].out_data_buffs[i] ].stateX[index] +=  scenarios[scenario_id].data_buff_cfg[ kernels[kernel_id].out_data_buffs[i] ].p_rate;
        data_buffers_redef[ kernels[kernel_id].out_data_buffs[i] ].total +=  scenarios[scenario_id].data_buff_cfg[ kernels[kernel_id].out_data_buffs[i] ].p_rate;
	}
}

bool kernel_control_tokens_available(int kernel_id)
{
	// If actor has control inputs
    // This has to be changed for auto-concurrency

    if(control_buffers[kernels[kernel_id].inp_ctl_buff].len &lt; 1)
	{
		return false;
	}
	else
	{
		return true;
	}
}


void detector_produce_control_tokens(int scenario_id)
{
	int i=0;
    int j=0;
	
	for(i=0; i &lt; NUM_CTL_BUFFS; i++ )
	{
        for(j=0; j &lt; scenarios[scenario_id].ctl_buff_cfg[ i ].p_rate; j++)
        {
		    control_buffers[i].queue[control_buffers[i].len++] = scenario_id;
        }
	}
}

void kernel_consume_control_tokens(int kernel_id)
{
	int i=0;
	
	
	control_buffers[kernels[kernel_id].inp_ctl_buff].len -= 1;
	
	while(i &lt; control_buffers[kernels[kernel_id].inp_ctl_buff].len)
	{
			control_buffers[kernels[kernel_id].inp_ctl_buff].queue[i] = control_buffers[kernels[kernel_id].inp_ctl_buff].queue[i+1];
			i++;
	}
	control_buffers[kernels[kernel_id].inp_ctl_buff].queue[i] = 0;
	
	
}

int kernel_get_curr_scenario_id(int kernel_id)
{
   // Needs to be changed to host auto-concurrency
   // Not the one from the head, not the tail, but???? img_len
   return control_buffers[kernels[kernel_id].inp_ctl_buff].queue[0];
}

int kernel_get_curr_scenario_delay(int kernel_id, int scenario_id)
{
	return scenarios[scenario_id].kernel_delay[kernel_id];
}

int detector_get_curr_scenario_delay(int scenario_id)
{
	return scenarios[scenario_id].detector_delay;
}

// Notice that scenario_id is passed by reference
bool kernel_tokens_available(int kernel_id)
{
    bool ret = false;
    int scenario_id;

    if(kernel_control_tokens_available(kernel_id)==true)
    {
        scenario_id = kernel_get_curr_scenario_id(kernel_id);
        // But we also need it here
        if(kernel_data_tokens_available(kernel_id, scenario_id)==true)
        {
            ret=true;
        }
        else
        {
            ret=false;
        }
    }else
    {
        ret=false;
    }
    return ret;
}

// Passed by reference for future use
void kernel_start_firing(int kernel_id, int&amp; scenario_id, int&amp; delay)
{

    scenario_id = kernel_get_curr_scenario_id(kernel_id);
    delay=kernel_get_curr_scenario_delay(kernel_id,scenario_id);

    kernel_consume_control_tokens(kernel_id);
    kernel_consume_data_tokens(kernel_id,scenario_id);   
}

void kernel_end_firing(int kernel_id, int scenario_id)
{
    kernel_produce_data_tokens(kernel_id, scenario_id);
}


bool detector_tokens_available(int scenario_id)
{
    return detector_data_tokens_available(scenario_id);
}

// Passed by reference so it can be used later
void detector_start_firing(int scenario_id, int&amp;delay)
{
    detector_consume_data_tokens(scenario_id);
    delay = detector_get_curr_scenario_delay(scenario_id);
}


void detector_end_firing(int scenario_id)
{
    detector_produce_control_tokens(scenario_id);
}

//}</declaration>
	<template>
		<name x="9" y="9">Kernel</name>
		<parameter>const int &amp;kernel_id, urgent chan &amp;self_timed__</parameter>
		<declaration>// Place local declarations here.
clock x;
int scenario_id;
int delay;</declaration>
		<location id="id0" x="442" y="119">
			<name x="432" y="85">Fire</name>
			<label kind="invariant" x="432" y="136">x &lt;= delay</label>
		</location>
		<location id="id1" x="221" y="-144">
			<name x="211" y="-178">Initial</name>
		</location>
		<init ref="id1"/>
		<transition>
			<source ref="id1"/>
			<target ref="id0"/>
			<label kind="guard" x="25" y="-59">kernel_tokens_available(kernel_id)</label>
			<label kind="synchronisation" x="93" y="93">self_timed__!</label>
			<label kind="assignment" x="85" y="127">kernel_start_firing(kernel_id, scenario_id, delay),
x:=0</label>
			<nail x="34" y="119"/>
		</transition>
		<transition>
			<source ref="id0"/>
			<target ref="id1"/>
			<label kind="guard" x="365" y="-34">x==delay</label>
			<label kind="assignment" x="365" y="-17">kernel_end_firing(kernel_id, scenario_id)</label>
			<nail x="323" y="-25"/>
		</transition>
	</template>
	<template>
		<name>Detector</name>
		<declaration>urgent broadcast chan self_timed__;
clock x;
int delay;</declaration>
		<location id="id2" x="391" y="561">
			<label kind="invariant" x="381" y="578">x &lt;= delay</label>
		</location>
		<location id="id3" x="399" y="246">
			<label kind="invariant" x="389" y="263">x &lt;= delay</label>
		</location>
		<location id="id4" x="391" y="-119">
			<label kind="invariant" x="381" y="-102">x&lt;=delay</label>
		</location>
		<location id="id5" x="374" y="-493">
			<label kind="invariant" x="364" y="-476">x&lt;=delay</label>
		</location>
		<location id="id6" x="-552" y="595">
			<label kind="invariant" x="-562" y="612">x&lt;=delay</label>
		</location>
		<location id="id7" x="-544" y="238">
			<label kind="invariant" x="-554" y="255">x&lt;=delay</label>
		</location>
		<location id="id8" x="-552" y="-153">
			<label kind="invariant" x="-562" y="-136">x&lt;=delay</label>
		</location>
		<location id="id9" x="-518" y="-544">
			<label kind="invariant" x="-528" y="-527">x&lt;=delay</label>
		</location>
		<location id="id10" x="-1292" y="43">
			<label kind="invariant" x="-1300" y="60">x &lt;= delay</label>
		</location>
		<location id="id11" x="76" y="578">
			<name x="67" y="544">P99</name>
		</location>
		<location id="id12" x="34" y="247">
			<name x="26" y="213">P70</name>
		</location>
		<location id="id13" x="43" y="-136">
			<name x="34" y="-170">P50</name>
		</location>
		<location id="id14" x="25" y="-501">
			<name x="17" y="-535">P30</name>
		</location>
		<location id="id15" x="-773" y="595">
			<name x="-781" y="561">P80</name>
		</location>
		<location id="id16" x="-799" y="238">
			<name x="-807" y="204">P60</name>
		</location>
		<location id="id17" x="-782" y="-144">
			<name x="-790" y="-178">P40</name>
		</location>
		<location id="id18" x="-799" y="-527">
			<name x="-807" y="-561">P0</name>
		</location>
		<location id="id19" x="-1623" y="43">
			<name x="-1632" y="9">I99</name>
		</location>
		<init ref="id19"/>
		<transition>
			<source ref="id3"/>
			<target ref="id19"/>
			<label kind="guard" x="417" y="480">x==delay</label>
			<label kind="assignment" x="417" y="514">detector_end_firing(6)</label>
			<nail x="586" y="782"/>
			<nail x="76" y="892"/>
			<nail x="-935" y="850"/>
		</transition>
		<transition>
			<source ref="id2"/>
			<target ref="id19"/>
			<label kind="guard" x="-109" y="633">x==delay</label>
			<label kind="assignment" x="-109" y="667">detector_end_firing(8)</label>
			<nail x="-127" y="773"/>
			<nail x="-841" y="782"/>
		</transition>
		<transition>
			<source ref="id4"/>
			<target ref="id19"/>
			<label kind="guard" x="-297" y="-884">x==delay</label>
			<label kind="assignment" x="-297" y="-850">detector_end_firing(4)</label>
			<nail x="544" y="-544"/>
			<nail x="-544" y="-867"/>
			<nail x="-1326" y="-518"/>
		</transition>
		<transition>
			<source ref="id5"/>
			<target ref="id19"/>
			<label kind="guard" x="-569" y="-714">x==delay</label>
			<label kind="assignment" x="-569" y="-680">detector_end_firing(2)</label>
			<nail x="-552" y="-765"/>
			<nail x="-1292" y="-433"/>
		</transition>
		<transition>
			<source ref="id11"/>
			<target ref="id2"/>
			<label kind="guard" x="94" y="535">detector_tokens_available(8)</label>
			<label kind="synchronisation" x="94" y="552">self_timed__!</label>
			<label kind="assignment" x="94" y="569">detector_start_firing(8,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id12"/>
			<target ref="id3"/>
			<label kind="guard" x="52" y="212">detector_tokens_available(6)</label>
			<label kind="synchronisation" x="52" y="229">self_timed__!</label>
			<label kind="assignment" x="52" y="246">detector_start_firing(6,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id13"/>
			<target ref="id4"/>
			<label kind="guard" x="61" y="-161">detector_tokens_available(4)</label>
			<label kind="synchronisation" x="61" y="-144">self_timed__!</label>
			<label kind="assignment" x="61" y="-127">detector_start_firing(4,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id14"/>
			<target ref="id5"/>
			<label kind="guard" x="43" y="-531">detector_tokens_available(2)</label>
			<label kind="synchronisation" x="43" y="-514">self_timed__!</label>
			<label kind="assignment" x="43" y="-497">detector_start_firing(2,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id6"/>
			<target ref="id11"/>
			<label kind="guard" x="-534" y="552">x==delay</label>
			<label kind="assignment" x="-534" y="586">detector_end_firing(7)</label>
		</transition>
		<transition>
			<source ref="id6"/>
			<target ref="id12"/>
			<label kind="guard" x="-534" y="387">x==delay</label>
			<label kind="assignment" x="-534" y="421">detector_end_firing(7)</label>
		</transition>
		<transition>
			<source ref="id6"/>
			<target ref="id13"/>
			<label kind="guard" x="-534" y="195">x==delay</label>
			<label kind="assignment" x="-534" y="229">detector_end_firing(7)</label>
		</transition>
		<transition>
			<source ref="id6"/>
			<target ref="id14"/>
			<label kind="guard" x="-534" y="13">x==delay</label>
			<label kind="assignment" x="-534" y="47">detector_end_firing(7)</label>
		</transition>
		<transition>
			<source ref="id15"/>
			<target ref="id6"/>
			<label kind="guard" x="-755" y="561">detector_tokens_available(7)</label>
			<label kind="synchronisation" x="-755" y="578">self_timed__!</label>
			<label kind="assignment" x="-755" y="595">detector_start_firing(7,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id7"/>
			<target ref="id11"/>
			<label kind="guard" x="-526" y="374">x==delay</label>
			<label kind="assignment" x="-526" y="408">detector_end_firing(5)</label>
		</transition>
		<transition>
			<source ref="id7"/>
			<target ref="id12"/>
			<label kind="guard" x="-526" y="208">x==delay</label>
			<label kind="assignment" x="-526" y="242">detector_end_firing(5)</label>
		</transition>
		<transition>
			<source ref="id7"/>
			<target ref="id13"/>
			<label kind="guard" x="-526" y="17">x==delay</label>
			<label kind="assignment" x="-526" y="51">detector_end_firing(5)</label>
		</transition>
		<transition>
			<source ref="id7"/>
			<target ref="id14"/>
			<label kind="guard" x="-526" y="-165">x==delay</label>
			<label kind="assignment" x="-526" y="-131">detector_end_firing(5)</label>
		</transition>
		<transition>
			<source ref="id16"/>
			<target ref="id7"/>
			<label kind="guard" x="-781" y="204">detector_tokens_available(5)</label>
			<label kind="synchronisation" x="-781" y="221">self_timed__!</label>
			<label kind="assignment" x="-781" y="238">detector_start_firing(5,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id8"/>
			<target ref="id11"/>
			<label kind="guard" x="-534" y="178">x==delay</label>
			<label kind="assignment" x="-534" y="212">detector_end_firing(3)</label>
		</transition>
		<transition>
			<source ref="id8"/>
			<target ref="id12"/>
			<label kind="guard" x="-534" y="13">x==delay</label>
			<label kind="assignment" x="-534" y="47">detector_end_firing(3)</label>
		</transition>
		<transition>
			<source ref="id8"/>
			<target ref="id13"/>
			<label kind="guard" x="-534" y="-178">x==delay</label>
			<label kind="assignment" x="-534" y="-144">detector_end_firing(3)</label>
		</transition>
		<transition>
			<source ref="id8"/>
			<target ref="id14"/>
			<label kind="guard" x="-534" y="-361">x==delay</label>
			<label kind="assignment" x="-534" y="-327">detector_end_firing(3)</label>
		</transition>
		<transition>
			<source ref="id17"/>
			<target ref="id8"/>
			<label kind="guard" x="-764" y="-182">detector_tokens_available(3)</label>
			<label kind="synchronisation" x="-764" y="-165">self_timed__!</label>
			<label kind="assignment" x="-764" y="-148">detector_start_firing(3,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id9"/>
			<target ref="id11"/>
			<label kind="guard" x="-500" y="-17">x==delay</label>
			<label kind="assignment" x="-500" y="17">detector_end_firing(1)</label>
		</transition>
		<transition>
			<source ref="id9"/>
			<target ref="id12"/>
			<label kind="guard" x="-500" y="-182">x==delay</label>
			<label kind="assignment" x="-500" y="-148">detector_end_firing(1)</label>
		</transition>
		<transition>
			<source ref="id9"/>
			<target ref="id13"/>
			<label kind="guard" x="-500" y="-374">x==delay</label>
			<label kind="assignment" x="-500" y="-340">detector_end_firing(1)</label>
		</transition>
		<transition>
			<source ref="id9"/>
			<target ref="id14"/>
			<label kind="guard" x="-467" y="-578">x==delay</label>
			<label kind="assignment" x="-450" y="-561">detector_end_firing(1)</label>
		</transition>
		<transition>
			<source ref="id18"/>
			<target ref="id9"/>
			<label kind="guard" x="-781" y="-569">detector_tokens_available(1)</label>
			<label kind="synchronisation" x="-781" y="-552">self_timed__!</label>
			<label kind="assignment" x="-781" y="-535">detector_start_firing(1,delay),
x:=0</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id15"/>
			<label kind="guard" x="-1275" y="264">x==delay</label>
			<label kind="assignment" x="-1275" y="298">detector_end_firing(0)</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id16"/>
			<label kind="guard" x="-1275" y="102">x==delay</label>
			<label kind="assignment" x="-1275" y="136">detector_end_firing(0)</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id17"/>
			<label kind="guard" x="-1275" y="-85">x==delay</label>
			<label kind="assignment" x="-1275" y="-51">detector_end_firing(0)</label>
		</transition>
		<transition>
			<source ref="id10"/>
			<target ref="id18"/>
			<label kind="guard" x="-1113" y="-221">x==delay</label>
			<label kind="assignment" x="-1275" y="-204">detector_end_firing(0)</label>
		</transition>
		<transition>
			<source ref="id19"/>
			<target ref="id10"/>
			<label kind="guard" x="-1606" y="9">detector_tokens_available(0)</label>
			<label kind="synchronisation" x="-1606" y="26">self_timed__!</label>
			<label kind="assignment" x="-1606" y="43">detector_start_firing(0,delay),
x:=0</label>
		</transition>
	</template>
	<system>
const int VLD_id=0;
const int IDCT_id=1;
const int RC_id=2;
const int MC_id=3;

urgent broadcast chan mc, rc, vld, idct;

VLD = Kernel(VLD_id,vld);
IDCT = Kernel(IDCT_id,idct);
RC = Kernel(RC_id,rc);
MC = Kernel(MC_id,mc);

FD=Detector();


system VLD, IDCT, RC, MC, FD;
</system>
	<queries>
		<query>
			<formula>A[] not deadlock
			</formula>
			<comment>
			</comment>
		</query>
		<query>
			<formula>E&lt;&gt; RC.Fire &amp;&amp; MC.Fire
			</formula>
			<comment>
			</comment>
		</query>
		<query>
			<formula>E&lt;&gt; VLD.Fire &amp;&amp; MC.Fire
			</formula>
			<comment>
			</comment>
		</query>
	</queries>
</nta>
